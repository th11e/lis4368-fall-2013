<!DOCTYPE html>
<html>
    <head>
        <title>Welcome to the book search!</title>
        <link href="book.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <?php
        session_start();
        include('divbox.php');
        div();
        ?>

        <?php if (isset($_SESSION['is_logged_in'])) { ?>
            Welcome! What would you like to do?
            <p><a href='https://ispace-2013.cci.fsu.edu/~th11e/lis4368/sellyourbook.php'>List a book.</a></p>
            <p><a href='https://ispace-2013.cci.fsu.edu/~th11e/lis4368/searchforbook.php'>Search for a book.</a></p>

        <?php } else { ?>

            <p>Welcome! You are not logged in! What would you like to do?</p>

            <p><a href='https://ispace-2013.cci.fsu.edu/~th11e/lis4368/searchforbook.php'>Search for a book.</a></p>

            Did you know? Members can list books as well! <br>
                <p>Please register by clicking 
                    <a href='https://ispace-2013.cci.fsu.edu/~th11e/lis4368/register.php'>HERE</a>.</p>

                <?php
            }
            ?>
            </div>
    </body>
</html>