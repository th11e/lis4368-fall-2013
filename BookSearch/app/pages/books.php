<h1>List of Books</h1>

<?php

//include('../model_mappers/bookmapper.php');
$mapper = new BookMapper($db);
$books = $mapper->retrieveBooks();

?>
        <?php
            
            echo "<table><tbody>";
            foreach($books as $book) {
                
                echo "<tr>";
                echo "<td>" . $book->getBookTitle() . " " . $book->getAuthor() . "</td>";
                echo "</tr>";
            }
            echo "</tbody></table>";
        ?>