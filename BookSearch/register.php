<!DOCTYPE html>
<html>
    <head>
        <title>Register</title>
        <link href="book.css" rel="stylesheet" type="text/css">
        <style type='text/css'>
            .error {
                color: #990000;
                font-weight: bold;
            }
        </style>
    </head>
    <body>


        <?php
        include('models/function.php'); //Includes function.php coding
        include('models/members.php'); //Includes member.php coding
        //Creates an array for validation errors
        $validationErrors = array();


        //If the POST array is not empty, process the form.
        if (count($_POST) > 0) {

            //Taken from class.
            //Heh heh.
            //First Name Validations
            //If there is a character that isn't accepted, too short, or too long, 
            //print out an error message.
            $result = checkAcceptChar($_POST['firstName']);
            if ($result === false) {
                $validationErrors[] = "The first name contains unacceptable characters.";
            }

            $result = checkLengthGreaterThan($_POST['firstName']);
            if ($result === false) {
                $validationErrors[] = "The first name is too short.";
            }

            $result = checkLengthLessThan($_POST['firstName']);
            if ($result === false) {
                //If the string is not less than 75, echo the statement.
                $validationErrors[] = "The first name is too long.";
            }

            //Lastname
            //If there is a character that isn't accepted, too short, or too long, 
            //print out an error message.
            $result = checkAcceptChar($_POST['lastName']);
            if ($result === false) {
                $validationErrors[] = "The last name contains unacceptable characters.";
            }

            $result = checkLengthGreaterThan($_POST['lastName']);
            if ($result === false) {
                $validationErrors[] = "The last name is too short.";
            }

            $result = checkLengthLessThan($_POST['lastName']);
            if ($result === false) {
                $validationErrors[] = "The last name is too long.";
            }

            //Password
            //If the password entered does not contain a number, doesn't match, 
            //is less than 7 characters, or greater than 20 characters, print 
            //an error message.
            $number = preg_match('@[0-9]@', $_POST['password']);
            if ($result === !$number) {
                $validationErrors[] = "The password needs to contain at least one number.";
            }

            $result = checkLengthLessThan($_POST['password'], 20);
            if ($result === false) {
                $validationErrors[] = "Password is too long!";
            }

            $result = checkLengthGreaterThan($_POST['password'], 7);
            if ($result === false) {
                $validationErrors[] = "The password too short.";
            }

            if ($_POST['password'] !== $_POST['re_password']) {
                $validationErrors[] = "Passwords must match.";
            }
            
            //Username is required.
             if (empty($_POST["username"])) {
                $validationErrors[] = "Username is required.";
             }

            //If there are no validation errors, printa success message.
            if (count($validationErrors) == 0) {

            //Creates object from typed-in data
            $newMember = new Member($_POST['firstName'], $_POST['lastName'], 
            $_POST['username'], $_POST['password'], $_POST['re_password'], 
            $_POST['phoneNumber'], $_POST['email'], $_POST['school']);

            $successMesg = "Welcome, " . $newMember->getFirstName() . ' ' . $newMember->getLastName() .
            "! What do you want to do? " .
            "<p><a href='https://ispace-2013.cci.fsu.edu/~th11e/lis4368/sellyourbook.php'>List a book.</a> - 
            <a href='https://ispace-2013.cci.fsu.edu/~th11e/lis4368/searchforbook.php'>Search for a book.</a></p>";
            }
        }
        
        include('divbox.php'); //Includes divbox.php coding
        div(); //Calls divbox.php function
        ?>

        <?php
        //Print the success message message
        if (isset($successMesg)) {
            echo $successMesg;
        }
        //Else, print the error message
        else {
            foreach ($validationErrors as $error) {
                echo "<p class='error'>" . $error . "</p>";
            }
            ?>

            <form action='register.php' method='post'>

                <p>
                    <label for='firstName_input'>First Name: </label> 
                    <input name ='firstName' type ='text' id='firstName_input' />
                </p>

                <p>
                    <label for='lastName_input'>Last Name: </label> 
                    <input name ='lastName' type ='text' id='lastName_input' />
                </p>

                <p>
                    <label for='username_input'>Username: </label> 
                    <input name ='username' type ='text' id='username_input'"/>
                </p>

                <p>
                    <label for='password_input'>Password: </label> 
                    <input name ='password' type ='password' id='password_input' />
                    <br><i>[Must contain at least one number.]</i>
                </p>


                <p>
                    <label for='re_password_input'>Re-Enter Password: </label> 
                    <input name ='re_password' type ='password' id='re_password_input' />
                </p>

                <p>
                    <label for='phoneNumber_input'>Phone Number: </label> 
                    <input name ='phoneNumber' type ='text' id='phoneNumber_input' />
                </p>

                <p>
                    <label for='email_input'>e-Mail: </label> 
                    <input name ='email' type ='text' id='email_input' placeholder=' JohnDoe@gmail.com'/>
                </p>

                <p>
                    <label for='school_input'>School: </label> 
                    <input name ='school' type ='text' id='school_input' />
                </p>

                <button type='submit'>Register</button>
            </form>


    <?php
}
?>

    </div>
</body>
</html>