<?php

include('../models/member.php');
include('../models/book.php');
include('MemberMapper.php');

$mapper = new BookMapper();

$bookObj = new Book('Biology', 'Ben Steves');
$bookObj->setMajor('IT');
$bookObj->setLevel(3);

$cResult = $mapper->createBook($bookObj);

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Databases!</title>
    </head>
    <body>
        
        <h1>Adding a Book</h1>
        
        <?php
        
            if ($cResult === true) {
                echo "It worked!";
            }
            else {
                echo "Whoops something went wrong!";
            }
        
        ?>
    </body>
</html>