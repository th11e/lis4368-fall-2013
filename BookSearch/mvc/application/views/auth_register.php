<h2>Register:</h2>

<form method='post'>
    
    <?php echo validation_errors(); ?>
    
    <?php
    
        if (isset($bad_credentials)) {
            echo $bad_credentials;
        }
    ?>

                <p>
                    <label for='firstName'>First Name: </label> 
                    <input name ='firstName' type ='text' id='firstName' />
                </p>

                <p>
                    <label for='lastName'>Last Name: </label> 
                    <input name ='lastName' type ='text' id='lastName' />
                </p>

                <p>
                    <label for='username_input'>Username: </label> 
                    <input name ='username' type ='text' id='username' />
                </p>

                <p>
                    <label for='password_input'>Password: </label> 
                    <input name ='password' type ='password' id='password' />
                    <br><i>[Must be at least 8 characters.]</i>
                </p>


                <p>
                    <label for='re_password_input'>Re-Enter Password: </label> 
                    <input name ='re_password' type ='password' id='re_password' />
                </p>
             

                <p>
                    <label for='phoneNumber_input'>Phone Number: </label> 
                    <input name ='phoneNumber' type ='text' id='phoneNumber' />
                </p>

                <p>
                    <label for='email_input'>e-Mail: </label> 
                    <input name ='email' type ='text' id='email' placeholder=' JohnDoe@gmail.com' />
                </p>

                <p>
                    <label for='school_input'>School: </label> 
                    <input name ='school' type ='text' id='school' />
                </p>

                <button type='submit'>Register</button>
            </form>