<h1>List your book:</h1>

<form method='post'>

    <?php echo validation_errors(); ?>
    
    <?php
    
        if (isset($bad_credentials)) {
            echo $bad_credentials;
        }
    ?>
    <p>
        <label>Book Title: <input name='bookTitle' type='text' /></label>
    </p>

    <p>
        <label for='cover'>Cover: </label> 
        <input name ='cover' type ='text' id='cover' placeholder=' Hardback, Paper, Binder' />
    </p>

    <p>
        <label for='edition'>Edition: </label> 
        <input name ='edition' type ='text' id='edition'/>
    </p>

    <p>
        <label for='author'>Author: </label> 
        <input name ='author' type ='text' id='author' />
    </p>

    <p>
        <label for='description'>Description: </label> 
        <input name ='description' type ='text' id='description' placeholder=' Copy from book website'/>
    </p>

    <p>
        <label for='isbn'>ISBN: </label> 
        <input name ='isbn' type ='text' id='isbn' />
    </p>

    <p>
        Condition: <input type='text' name='condition'><br>
	[Like New, Very Good, Good, Acceptable, Fair]
    </p>

    <p>
        <label for='courseNumber'>Course Number: </label> 
        <input name ='courseNumber' type ='text' id='courseNumber' placeholder=' LIS4368' />
    </p>

    <p>
        <label for='cost'>Price: </label> 
        <input name ='cost' type ='text' id='cost' />
    </p>


<button type='submit'>List my book!</button>

</form><br>

<a href="https://ispace-2013.cci.fsu.edu/~th11e/4368/mvc/index.php/auth/index">Click here to return to index page!</a>