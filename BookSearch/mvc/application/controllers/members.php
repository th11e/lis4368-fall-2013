<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Members extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
               
        $this->load->helper('view');
        
        $this->load->model('member_model');
    }

    //List books in the database
    public function index()
    {
        //Load our book_model up
        
        //Run the retrieveBooks() method to get an array of books
        $memberList = $this->member_model->retrieveMembers();
        
        //Create an array of data to pass to the view (keys become variables)
        $viewData = array('members' => $memberList);
        
        //Pass the books array to a view to printed in HTML
        load_a_page('members_index', $viewData);
        
    }
    
}