<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function index() {

        if ($this->session->userdata('is_logged_in') != 'true') {
            redirect('auth/login');
        } else {

            load_a_page('auth_index');
        }
    }

    public function login() {
        //Create an array called viewData
        $viewData = array();

        //Helper is getting loaded.
        //Form Helper file contains functions that assist in working with forms.
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        //If any data was entered in the form..
        if (count($_POST) > 0) {

            //Sets rules for form validation (in the library). Using the entered
            //username as the login name. 
            $this->form_validation->set_rules('username', 'Login Name', 'required|max_length[16]');
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[8]');

            //Run the form validation
            $valResult = $this->form_validation->run();

            //If it runs..
            if ($valResult === true) {

                //Check the username and password against the database
                $this->load->model('user_model');
                $result = $this->user_model->checkCredentials($_POST['username'], $_POST['password']);

                //IF they match, then log the user in
                if ($result === true) {
                    $this->session->set_userdata('is_logged_in', 'true');
                    
                    
                    redirect('auth_index');
                } else {
                    //Reprint form
                    $viewData['bad_credentials'] = "We could not find your username and password. You must be logged in to view the next page.";
                }
            }
        }

        load_a_page('auth_login', $viewData);
    }

//------------------------------------------------------

    public function logout() { {
            load_a_page('auth_logout');
            $this->session->userdata = array();
            $this->session->sess_destroy();
        }
    }

//------------------------------------------------------

    public function register() {
        $viewData = array();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        //If any data was entered in the form..
        if (count($_POST) > 0) {

            //Sets rules for form validation (in the library).
            $this->form_validation->set_rules('firstName', 'First Name', 'required|min_length[2]');
            $this->form_validation->set_rules('lastName', 'Last Name', 'required|min_length[2]');
            $this->form_validation->set_rules('username', 'Username', 'required|min_length[3]|max_length[12]|trim');
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[2]|md5');
            $this->form_validation->set_rules('re_password', 'Re-Enter Password', 'required|min_length[2]|md5');
            $this->form_validation->set_rules('phoneNumber', 'Phone Number', 'max_length[10]');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|trim');
            $this->form_validation->set_rules('school', 'School', 'max_length[100]|trim');

            //Run the form validation
            $valResult = $this->form_validation->run();

            //If it runs..
            if ($valResult === true) {
                //Refers back to the user model to pull checkUsername
                $this->load->model('user_model');

                //This is where it checks to see if the username already exists
                //If it doesn't, then new data is inserted.
                $result = $this->user_model->checkUsername($_POST['username']);

                //If it returns false, then 
                if ($result === true) {

                    //Insert username into the database
                    $this->user_model->addMember();

                    //Then logs the person into the website.
                    $this->session->set_userdata('is_logged_in', 'true');

                    //Re-directs to default route (Books)
                    //Should choose a different location.
                    redirect('auth/index');
                } else {
                    //Reprint form                    
                    $viewData['bad_credentials'] = "That username is already in use.";
                }
            }
        }
        //Loads "auth/register"
        load_a_page('auth_register');
    }

}
