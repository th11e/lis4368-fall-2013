<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Books extends CI_Controller {

    public function __construct() {
        parent::__construct();



        $this->load->helper('view');
        $this->load->model('book_model');
    }

    //List books in the database
    public function index() {

        if ($this->session->userdata('is_logged_in') != 'true') {
            redirect('auth/login');
        } else {
            //Run the retrieveBooks() method to get an array of books
            $bookList = $this->book_model->retrieveBooks();

            //Create an array of data to pass to the view (keys become variables)
            $viewData = array('books' => $bookList);

            //Pass the books array to a view to printed in HTML
            load_a_page('books_index', $viewData);
        }
    }

        //------------------------------------------  


        public function add() {
            $viewData = array();
            $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');

            //If any data was entered in the form..
            if (count($_POST) > 0) {

                //Sets rules for form validation (in the library).
                $this->form_validation->set_rules('bookTitle', 'Book Title', 'required');
                $this->form_validation->set_rules('cover', 'Cover', 'required');
                $this->form_validation->set_rules('edition', 'Edition', '');
                $this->form_validation->set_rules('author', 'Author', 'required');
                $this->form_validation->set_rules('description', 'Description', '');
                $this->form_validation->set_rules('isbn', 'isbn', 'required');
                $this->form_validation->set_rules('condition', 'Condition', 'required');
                $this->form_validation->set_rules('courseNumber', 'Course Number', '');
                $this->form_validation->set_rules('cost', 'Cost', 'required');


                $this->load->model('book_model');


                $this->book_model->addBook();

                //Re-directs to default route (Books)
                //Should choose a different location.
                redirect('');
            }
            //Loads "auth/register"
            load_a_page('books_add');
        }

    }

    