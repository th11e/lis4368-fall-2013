<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class X extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function do_something()
    {
        if ($this->is_logged_in())
        {
            redirect ('auth_index');
        } else {redirect('auth_login');}
    }
}

