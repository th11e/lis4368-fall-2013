<?php

class User_model extends CI_Model {

    function __construct() {
        parent::__construct();
        
    }

    function checkCredentials($username, $password) {
        $this->load->database();

        $check = array(
            'username' => $username,
            'password' => $password
        );

        //Looks through the member database for a the login results.
        $query = $this->db->get_where('member', $check, 1);

        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }
        
//------------------------------------------------------
        
    function checkUsername($username) {
        
        //Loads the database
        $this->load->database();

        $check = array(
        'username' => $username
        );

        //Looks through the member database for a the login results.
        $query = $this->db->get_where('member', $check, 1);

        if ($query->num_rows() == 1) {

        //If there is already the username in the database, then return false
        //because there can not be duplicates.
        return false;
        } else {
        //When there isn't a username, return true so that it can be a new username
        return true;
        }
        }

        
//--------------------------------------------------------
        
    function addMember() {
            $data = array(
                'firstName' => $this->input->post('firstName'),
                'lastName' => $this->input->post('lastName'),
                'username' => $this->input->post('username'),
                'password' => md5($this->input->post('password')),
                're_password' => md5($this->input->post('re_password')),
                'phoneNumber' => $this->input->post('phoneNumber'),
                'email' => $this->input->post('email'),
                'school' => $this->input->post('school'),
            );
            
        //Load the database
        $this->load->database();
        
        //Insert into th11e_booksearch.member with values from $data
        $query = $this->db->insert('th11e_booksearch.member', $data);
        }

    }
