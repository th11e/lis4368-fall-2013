<?php

class Search_model extends CI_Model {
    
        function __construct() 
    {
        parent::__construct();
    }

function get_results($search_term='default')
    {
        
        $this->load->database();
        // Use the Active Record class for safer queries.
        $this->db->select('*');
        $this->db->from('th11e_booksearch.book');
        $this->db->like('bookTitle',$search_term);

        // Execute the query.
        $query = $this->db->get();

        // Return the results.
        return $query->result_array();
    }


}
