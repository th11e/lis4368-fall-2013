<?php
include('functions.php');
include('../class/students.php');

//Validation Errors Array
$validationErrors = array();

//If the POST array is not empty, process the form...
if (count($_POST) > 0) {


    //1. ACCEPT THE DATA (in $_POST)
    //2. VALIDATE THE DATA
    //Lastname
    $result = checkAcceptableCharacters($_POST['lastName']);
    if ($result === false) {
        $validationErrors[] = "The last name contains unacceptable characters";
    }

    $result = checkStringLengthGreaterThan($_POST['lastName']);
    if ($result === false) {
        $validationErrors[] = "The last name is too short!";
    }

    $result = checkStringLengthLessThan($_POST['lastName']);
    if ($result === false) {
        $validationErrors[] = "The last name is too long!";
    }

    //Firstname
    $result = checkAcceptableCharacters($_POST['firstName']);
    if ($result === false) {
        $validationErrors[] = "The first name contains unacceptable characters";
    }

    $result = checkStringLengthGreaterThan($_POST['firstName'], 2);
    if ($result === false) {
        $validationErrors[] = "The first name is too short!";
    }

    $result = checkStringLengthLessThan($_POST['firstName'], 75);
    if ($result === false) {
        $validationErrors[] = "The first name is too long!";
    }

    $result = checkStringLengthLessThan($_POST['password'], 51);
    if ($result === false) {
        $validationErrors[] = "The password is too long!";
    }

    $result = checkStringLengthGreaterThan($_POST['password'], 7);
    if ($result === false) {
        $validationErrors[] = "The password too short.";
    }

    $number = preg_match('@[0-9]@', $_POST['password']);
    if ($result === !$number) {
        $validationErrors[] = "The password needs to contain at least one number.";
    }

    if (count($validationErrors) == 0) {

        //3. CREATE AN OBJECT FROM THE DATA
        $newStudent = new Student($_POST['firstName'], $_POST['lastName']);

        //4. STORE THAT OBJECT IN THE DATBASE
        // -- For now, we'll echo it on the screen
        $successMesg = "<p>Name: " . $newStudent->getFirstName() . ' ' . $newStudent->getLastName() . "</p>";
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Forms!</title>

        <style type='text/css'>
            .error {
                color: #990000;
                font-weight: bold;
            }
        </style>
    </head>
    <body>

<?php
if (isset($successMesg)) {
    echo $successMesg;
} else {
    ?>

            <?php
            foreach ($validationErrors as $err) {
                echo "<p class='error'>" . $err . "</p>";
            }
            ?>

            <h1>Hello Forms!</h1>

            <form action='index.php' method='post'>

                <p>
                    <label>First Name: <input name='firstName' type='text' /></label>
                </p>

                <p>
                    <label for='last_name_input'>Last Name:</label>
                    <input name='lastName' type='text' id='last_name_input' />
                </p>

                <p>
                    <label for='password_input'>Password</label>
                    <input name='password' type='password' id='password_input' />
                </p>

                <p>
                    <label>Are you Enrolled?</label>
                    <input type='radio' name='enrolled' value='yes' /> Yes
                    <input type='radio' name='enrolled' value='no'  /> No
                </p>

                <p>
                    <label>Sign up for our Newsletter?</label>
                    <input type='checkbox' name='newsletter' value='true' /> Sure.
                </p>

                <p>
                    <select name='major_choice'>
                        <option value='02343'>IT</option>
                        <option value='23423'>Art</option>
                        <option value='12345'>Humanities</option>
                    </select>
                </p>

                <p>
                    <input type='date' name='birthdate' />
                </p>

                <p>
                    <input name='favorite_color' type='color' />
                </p>


                <button type='submit'>Sign Up</button>

            </form>

    <?php
}
?>

    </body>
</html>