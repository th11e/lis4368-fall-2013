<?php

session_start();

?>
<html>
  <head>
     <title>PHP Test</title>
  </head>
  <body>
  
      <h1>Stuff Page</h1>
  
      <?php if (isset($_SESSION['is_logged_in'])) { ?>
      
      <p>Hi Logged-in person.  Here is some special stuff for you.</p>
      
      <?php } else { ?>
      
      <p>You're anonymous.  Here is stuff for anonymous users.</p>
      
      <?php } ?>
  </body>
</html>