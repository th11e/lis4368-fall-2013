<?php

   function checkAcceptableCharacters($str)
   {
        $allowedChars = "ABCDEFGHIJKLMNOPQRSTUVWYZabcdefghijlkmnopqrstuvwxyz -'";
    
        for($i = 0; $i < strlen($str); $i++) {

            $currChar = $str{$i};
            $isValid = (strpos($allowedChars, $currChar) !== false);

            if ($isValid === false) {
                return false;
            }
        }
        
        //If made it here
        return true;
   }

   // -------------------------------------------------------------
   
   function checkStringLengthGreaterThan($str, $checkLength = 2)
   {
       return (strlen($str) > $checkLength);
   }
   
   // -------------------------------------------------------------   
   
   function checkStringLengthLessThan($str, $checkLength = 75)
   {
       return (strlen($str) < $checkLength);
   }

?>
