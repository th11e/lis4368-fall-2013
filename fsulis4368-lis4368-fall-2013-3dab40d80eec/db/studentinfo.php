<?php

include('../oop/models/student.php');
include('../oop/models/course.php');
include('StudentMapper.php');

$mapper = new StudentMapper();

$student = $mapper->retrieveStudent(45);

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Databases!</title>
    </head>
    <body>
        
        <h1>Connect to MySQL and get list of students!</h1>
        
        <?php
            
            echo "<table><tbody>";
            echo "<tr>";
            echo "<td>" . $student->getFirstName() . " " . $student->getLastName() . "</td>";
            echo "</tr>";
            echo "</tbody></table>";
        ?>
        
    </body>
</html>
