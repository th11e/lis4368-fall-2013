<?php

include('../oop/models/student.php');
include('../oop/models/course.php');
include('StudentMapper.php');
include('dbconn.php');

$db = new Dbconn('ispace-2013.cci.fsu.edu', 'cam02h_lis4368_fall2013', 'cam02h', 'hmfwztr9');

$mapper = new StudentMapper($db);

$students = $mapper->retrieveStudents();

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Databases!</title>
    </head>
    <body>
        
        <h1>Connect to MySQL and get list of students!</h1>
        
        <?php
            
            echo "<table><tbody>";
            foreach($students as $student) {
                
                echo "<tr>";
                echo "<td>" . $student->getFirstName() . " " . $student->getLastName() . "</td>";
                echo "</tr>";
            }
            echo "</tbody></table>";
        ?>
        
    </body>
</html>
