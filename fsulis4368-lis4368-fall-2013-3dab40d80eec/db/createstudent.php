<?php

include('../oop/models/student.php');
include('../oop/models/course.php');
include('StudentMapper.php');

$mapper = new StudentMapper();

$studentObj = new Student('Jeb', 'Smith');
$studentObj->setMajor('IT');
$studentObj->setLevel(3);

$cResult = $mapper->createStudent($studentObj);

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Databases!</title>
    </head>
    <body>
        
        <h1>Creating a Student</h1>
        
        <?php
        
            if ($cResult === true) {
                echo "It worked!";
            }
            else {
                echo "Whoops something went wrong!";
            }
        
        ?>
    </body>
</html>
