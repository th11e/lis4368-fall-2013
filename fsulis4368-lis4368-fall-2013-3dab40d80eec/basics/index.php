<!DOCTYPE html>
<html>
    <head><title>Hi</title></head>
    <body>
        <h1>Hello World!</h1>

        <?php
        $myvariable = "Hi there world";

        echo $myvariable . "\n";
        echo "<br/>\n";
        echo $myvariable . "\n";

        //Variable types
        // SCALAR Variables
        $string = "Hi there ya'll";
        $integer = 15;
        $float = 16.04;
        $boolean = false;
        $null    = null;

        $integer = "What up";
        $sum = $integer + $float;

        $item = "true";

        echo gettype($item);
        
        
        $myNumber = (float) 15;
        $myValue  = (int) 46.234;
        
        echo $myValue;
        
        $myValue = (boolean) "bobby";
        
        echo $myValue;
        
        ?>
    </body>
</html>