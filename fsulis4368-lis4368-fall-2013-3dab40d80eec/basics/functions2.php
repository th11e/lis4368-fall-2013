<?php

//
// GLOBAL SCOPE
//

$a = 'apple';
$b = 'banana';
$c = 'cherry';

// ---------------------------------------------------------

function howAboutSomeFruit($whatFruit)
{
    //
    // LOCAL SCOPE for this function
    //
    
    return "You are eating a " . $whatFruit;
}

// ---------------------------------------------------------

function paragraphize($str)
{
    echo "<p>";
    echo howAboutSomeFruit($str);
    echo "</p>";
}

// ---------------------------------------------------------

paragraphize($a);
?>