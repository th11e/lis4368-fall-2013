<?php


//For loop
//While
//Do..while


//While

$myInt = 0;

while ($myInt < 15) {
    echo "<p>myInt is " . $myInt . "</p>";
    $myInt = $myInt + 1;
}

echo "<p>All done</p>";

// -----------------------------------------

//For loop

for ($myInt = 0; $myInt < 15; $myInt++) {
    echo "<p>myInt is " . $myInt . "</p>";     
}

echo "<p>All done</p>";

// -----------------------------------------

//Do..while

$myInt = 0;

do {
    
    echo "<p>myInt is " . $myInt . "</p>";
    $myInt++;    
    
} while (45 < 15);


?>
