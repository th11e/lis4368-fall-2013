<?php

//Comparison Operators

if (5 == 5) {
    echo "The universe works";
}

if (false == "") {
    echo "things are false";
}

$myNumber = 0;

if ($myNumber == false) {
    echo "Zero is equal to false";
}
else {
    echo "Zero is not equal to false.";
}

if ((float) 15 === 15.0) {
    echo "15 equals 15";
}

?>
