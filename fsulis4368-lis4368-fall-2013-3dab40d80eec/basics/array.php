<?php

//INCREMENTAL ARRAYS
//They are just okay

//Aren't they?

$myArray = array('red', 'green', 'blue');

var_dump($myArray);

//echo 'green'
echo $myArray[1];
//echo 'blue'
echo $myArray[2];

//??
echo $myArray[3];

//Overwrite green with yellow
$myArray[1] = 'yellow';

//add 'white' to the end of the array
$myArray[3] = 'white';

var_dump($myArray);

//Count items in the array
$numColors = count($myArray);

echo "<p>" . $numColors . "</p>";

//Add items onto the end of the array without worrying about count
$myArray[] = "Black";
$myArray[] = "Orange";
$myArray[] = "Aqua";
$myArray[] = "Mauve";

var_dump($myArray);

for ($i = 0; $i < count($myArray); $i++) {
    echo "<li>" . $myArray[$i] . "</li>";
    
}

// =================================================

// ASSOCIATIVE ARRAY

$courses = array('LIS4368' => 'Web Dev with PHP', 'LIS1234' => 'Intro to IT', 'ABC1324' => "Basketweaving");

var_dump($courses);

//Overwrite the value at index of "LIS4368"
$courses['LIS4368'] = "Web Development with PHP";
echo $courses['LIS4368'];

//Add new items to the array
$courses['DIS5555'] = "Directed independent study";
//$courses[] = "Some course";

var_dump($courses);

//Your keys can be any SCALAR type
$stuff = array(1.1 => "A", 5.5 => 'B', 1.3 => "C");
$morestuff = array(3 => "A", 55 => "B", 103423 => "C");

//Mixed type arrays
$myMixedTypeArray = array('abc', 15, 44.5, false, "WHATUP", array(1,2,3));

var_dump($myMixedTypeArray);

//Print out associative array -- WON'T WORK!!
for ($i = 0; $i < count($courses); $i++) {
    echo "<li>" . $courses[$i] . "</li>";
}

//Instead we'll use a foreach
//Syntax is (arrayVariable as 
foreach ($courses as $courseNum => $courseVal) {
    echo "<li>" . $courseNum . ": " . $courseVal . "</li>";
}

echo "<hr/><h1>Multidimensional Arrays</h1>";

//A multidimensional array (incremental arrays)
$courses = array(
    array('LIS4368', 'Web Dev with PHP'),
    array('LIS1234', 'Intro to IT'),
    array('ABC1234', 'Basketweaving')
);

//A multidimensional array (associative arrays)
$students = array(
    '#1253' => array('fname' => 'Bob',  'lname' => 'Morris'),
    '#5555' => array('fname' => 'Greg', 'lname' => 'Smith')
);

//Printing a multidimensional array (nested foreach loops)
echo "<table><tbody>";
foreach ($courses as $course) {
    
    echo '<tr>';
    
    foreach ($course as $courseData) {
        echo '<td>' . $courseData . '</td>';
    }
    
    echo '</tr>';
    
}
echo "</tbody></table>";


?>
