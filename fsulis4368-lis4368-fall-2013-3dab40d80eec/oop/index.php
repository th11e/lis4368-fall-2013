<?php

    include('models/student.php');
    include('models/course.php');

    //Students array
    $students = array();

    //Instantiating a class into an object and putting it into an array
    $students[] = new Student('Bob', 'McStudents');            
    $students[] = new Student('Sally', 'McStudent');
    $students[] = new Student('Tim', 'McPerson');
    $students[] = new Student('Wanda', 'McPerson');
    $students[] = new Student('Jake', 'McPerson');
    $students[] = new Student('Megan', 'McPerson');
    $students[] = new Student('Molly', 'McPerson');
    $students[] = new Student('Patrick', 'McPerson');
    $students[] = new Student('Greg', 'McPerson');

    //Courses
    $courses = array();
    
    $courses[] = new Course('PHP Web Dev', 'LIS4368');
    $courses[] = new Course('Intro to IT', 'LIS1234');
    $courses[] = new Course('Basketweaving', 'ABC1234');
    
    //Shuffle the students
    shuffle($students);
    
    //Assign students to rosters round-robin
    $courseIndex = 0;    
    foreach($students as $person) {
                
        //Add the current student to the course
        $courses[$courseIndex]->addStudent($person);
        
        if (isset($courses[$courseIndex + 1])) {
            $courseIndex++;          
        }
        else {
            $courseIndex = 0;
        }
        
    }
    
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>My Page - Happy Now?</title>
    </head>
    <body>
        <h1>Hello, There!</h1>
        
        <?php

            echo "<ul>";
            
            foreach($students as $stud) {
                echo "<li>" . $stud->getFirstName() . " " . $stud->getLastName() . "</li>"; 
            }
            echo "</ul>";
            
         ?>
        
        <h1>Courses</h1>
 
        <?php

            echo "<ul>";
            
            foreach($courses as $course) {
                echo "<li>" . $course->name . " " . $course->number . "</li>"; 
                
                echo "<ul>";
                foreach($course->roster as $student) {
                    echo "<li>" . $student->getFirstName() . ' ' . $student->getLastName() . "</li>";
                }
                echo '</ul>';
            }
            echo "</ul>";
            
         ?>        
        
    </body>
</html>
