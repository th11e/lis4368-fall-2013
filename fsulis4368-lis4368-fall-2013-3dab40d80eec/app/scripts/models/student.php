<?php

class Student
{
    //Class Properties
    protected $firstName;
    protected $lastName;
    protected $id;
    protected $major;
    protected $level;
    
    //Magic Constructor Method
    //Never use a return statement here.
    public function __construct($firstName = null, $lastName = null)
    {
        if ($firstName !== null) {
            $this->setFirstName($firstName);
        }
        
        if ($lastName !== null) {
            $this->setLastName($lastName);
        }
    }
    
    //Interface Methods    
    public function setMajor($major)
    {
        $this->major = $major;
    }
    
    public function getMajor()
    {
        return $this->major;
    }
    
    public function getLevel()
    {
        return $this->level;
    }
    
    public function setLevel($level)
    {
        $this->level = $level;
    }
    
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }
    
    public function getLastName()
    {
        return $this->lastName;
    }
    
    public function setFirstName($firstName)
    {
        if (strtolower($firstName) == 'john') {
            user_error("JOHN IS NOT ALLOWED");
        }
        
        if (gettype($firstName) == 'string') {
            $this->firstName = $firstName;            
        }
        else {
            user_error("ERROR: YOU DIDN'T SEND IN A STRING. BOO");
        }
    }
    
    public function getFirstName()
    {
        return $this->firstName;
    }
    
    //Other Class Methods
    public function printMyName()
    {
        return $this->firstName . " " . $this->lastName;
        $this->doSomething();
    }
    
    public function attendLecture($courseName)
    {
        return "I am going to attend lecture in: " . $courseName;
    }
    
    public function checkSchdule()
    {
        echo "Checking my schedule";
    }
    
    public function changeMajor()
    {
        echo "I'll be changing my major now";
    }
    
    protected function doSomething()
    {
        echo "HEY YA'LL";
    }
}

?>