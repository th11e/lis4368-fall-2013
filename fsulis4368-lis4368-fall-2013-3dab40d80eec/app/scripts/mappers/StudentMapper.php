<?php

class StudentMapper
{
    protected $dbconn;
    
    function __construct($dbconn)
    {
        $this->dbconn = $dbconn;
    }
    
    function createStudent($studentObj)
    {
        //Connect to database by creating PDO object
        $conn = new PDO('mysql:host=ispace-2013.cci.fsu.edu;dbname=cam02h_lis4368_fall2013', 'cam02h', 'hmfwztr9');
                
        $stmt = $conn->prepare("INSERT INTO student (firstName, lastName, major, level) VALUES (:fname, :lname, :major, :level)");
        $stmt->bindParam(':fname', $studentObj->getFirstName());
        $stmt->bindParam(':lname', $studentObj->getLastName());        
        $stmt->bindParam(':major', $studentObj->getMajor());
        $stmt->bindParam(':level', $studentObj->getLevel());
        
        $result = $stmt->execute();
        
        if ($result === false) {
            var_dump($conn->errorCode());
        }
        
        return $result;
    }
    
    function retrieveStudents()
    {
        $conn = $this->dbconn->getConnection();

        //Run a query
        $result = $conn->query('SELECT * FROM student;');      
        
        //Results from the databse will be converted into Student objects
        $result->setFetchMode(PDO::FETCH_CLASS, 'Student');           
        $students = $result->fetchAll(); 
        
        return $students;
    }
    
    function retrieveStudent($id)
    {
        //Connect to database by creating PDO object
        $conn = $this->dbconn->getConnection();
        
        //Run a query
        $result = $conn->query("SELECT * FROM student WHERE id = $id;");      
        
        //Results from the databse will be converted into Student objects
        $result->setFetchMode(PDO::FETCH_CLASS, 'Student');           
        $students = $result->fetch(); 
        
        return $students;        
    }
    
    function updateStudent()
    {
        
    }
    
    function deleteStudent()
    {
        
    }
}


?>