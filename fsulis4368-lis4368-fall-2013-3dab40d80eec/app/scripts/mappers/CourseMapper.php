<?php

class CourseMapper
{
    function createCourse($courseObj)
    {
        // //Connect to database by creating PDO object
        //$conn = $this->connectToDatabase();
     
        // $stmt = $conn->prepare("INSERT INTO student (firstName, lastName, major, level) VALUES (:fname, :lname, :major, :level)");
        // $stmt->bindParam(':fname', $studentObj->getFirstName());
        // $stmt->bindParam(':lname', $studentObj->getLastName());        
        // $stmt->bindParam(':major', $studentObj->getMajor());
        // $stmt->bindParam(':level', $studentObj->getLevel());
        
        // $result = $stmt->execute();
        
        // if ($result === false) {
        //     var_dump($conn->errorCode());
        // }
        
        // return $result;
    }
    
    function retrieveCourses()
    {
        $conn = $this->connectToDatabase();

        //Run a query
        $result = $conn->query('SELECT * FROM course;');      
        
        //Results from the databse will be converted into Student objects
        $result->setFetchMode(PDO::FETCH_CLASS, 'Course');           
        $courses = $result->fetchAll(); 
        
        return $courses;
    }
    
    function retrieveCourse($id)
    {
        $conn = $this->connectToDatabase();
        
        //Run a query
        $result = $conn->query("SELECT * FROM course WHERE id = $id;");      
        
        //Results from the databse will be converted into Student objects
        $result->setFetchMode(PDO::FETCH_CLASS, 'Course');           
        $course = $result->fetch(); 
        
        return $course;        
    }
    
    protected function connectToDatabase()
    {
        //Connect to database by creating PDO object
        $conn = new PDO('mysql:host=ispace-2013.cci.fsu.edu;dbname=cam02h_lis4368_fall2013', 'cam02h', 'hmfwztr9');
        return $conn;  
    }
}


?>