<?php

  //Force PHP to show errors on the screen (development only)
  error_reporting(E_ALL);

  // Determine which page the user wants to see, or default to front page
  if (isset($_GET['page'])) {
    $whichPage = $_GET['page'];
  }
  else {
    $whichPage = 'front';
  }
  
  //Load up the PHP resources that we are going to want to use across different
  //pages
  
  //Tell PHP to use sessions
  session_start();
  
  //Include Models and create a new database connection
  include('scripts/models/student.php');
  include('scripts/models/course.php');
  include('scripts/mappers/StudentMapper.php');
  include('scripts/libraries/dbconn.php');
  $db = new Dbconn('ispace-2013.cci.fsu.edu', 'cam02h_lis4368_fall2013', 'cam02h', 'hmfwztr9');
   
  
  //Include the header
  include('includes/header.php');
    
  // Load that page up if it exists, else... 404
  if (file_exists('pages/' . $whichPage . '.php')) {
    include('pages/' . $whichPage . '.php');
  }
  else {
    //Do a 404...
    header('HTTP/1.0 404 Not Found');
    include('includes/404.php');
  }

  //Include the footer
  include('includes/footer.php');

?>