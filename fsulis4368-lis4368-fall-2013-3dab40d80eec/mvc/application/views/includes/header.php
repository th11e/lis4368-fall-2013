<html>
  <head>
     <title>XYZ University</title>
     <link rel='stylesheet' href='<?php echo base_url(); ?>assets/style.css' type='text/stylesheet' />
  </head>
  <body>

    <h1>XYZ University Inc - Course Registration System</h1>
    <h2>The Best in the World</h2>
    
    <nav>
      <ul>
        <li><a href='<?php echo site_url(); ?>'>Home</a></li>
        <li><a href='<?php echo site_url('courses/index'); ?>'>Courses Index</a></li>
        <li><a href='<?php echo site_url('students/index'); ?>'>Students List</a></li>
      </ul>
    </nav>
    
    <hr />