<h2>Login to our System</h2>

<form method='post' action=''>
    
    <?php echo validation_errors(); ?>
    
    <?php
    
        if (isset($bad_credentials)) {
            echo $bad_credentials;
        }
    ?>
    
    <p>
        <label for='un'>Login</label>
        <input type='text' name='un' id='un' />
    </p>

    <p>
        <label for='pw'>Password</label>
        <input type='password' name='pw' id='pw' />   
    </p>
    
    <p>
        <button type='submit'>Login</button>
    </p>

</form>