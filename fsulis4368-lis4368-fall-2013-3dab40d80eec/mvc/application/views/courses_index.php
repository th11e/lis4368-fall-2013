    <h1>List of Courses!</h1>

    <table>
        <tbody>
            <?php foreach($courses as $course) { ?>
                <tr>
                    <td><?php echo $course->name; ?></td>
                    <td><?php echo $course->number; ?></td>
                    <td><?php echo $course->instructor; ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>