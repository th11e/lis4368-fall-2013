<?php

class Student_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    function createStudent($firstName, $lastName, $email)
    {
         $data = array(
           'firstName' => $firstName,
           'lastName'  => $lastName,
           'email'     => $email
        );
        
        return $this->db->insert('student', $data);        
    }
}