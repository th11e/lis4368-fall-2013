<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function login()
    {
        $viewData = array();
        
        $this->load->helper(array('form', 'url'));
    	$this->load->library('form_validation');

        if (count($_POST) > 0) {

    	    $this->form_validation->set_rules('un', 'Login Name', 'required|max_length[16]|alpha_dash');
    	    $this->form_validation->set_rules('pw', 'Password',   'required|min_length[8]');

            $valResult = $this->form_validation->run();
            
            if ($valResult === true) {
                
                //Check the username and password against the database
                $this->load->model('user_model');
                $result = $this->user_model->checkCredentials($_POST['un'], $_POST['pw']);
                
                //IF they match, then log the user in
                if ($result === true) {
                    $this->session->set_userdata('is_logged_in', 'true');
                    redirect('');
                } 
                else { //Reprint form
                    $viewData['bad_credentials'] = "We could not find your username and password.  Try again?";
                }
            }
        }
        
        load_a_page('auth_login', $viewData);
    }
    
    public function logout()
    {
        echo "This is logout";
    }
    
    public function register()
    {
        echo "This will be the registration form";
    }

}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */