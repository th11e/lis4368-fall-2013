<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Students extends CI_Controller {

    public function index()
    {
        $this->load->helper('view');
        
        //Pass the courses array to a view to printed in HTML
        load_a_page('students_index');
    }
    
    public function manage()
    {
        //Check to make sure the user is logged in, or redirect to login page
        if ($this->session->userdata('is_logged_in') != 'true') {
            redirect('auth/login');
        }
        
        //Load the page up
        load_a_page('students_manage');
    }
    
    public function single($id)
    {
        echo "single info";
    }
    
    public function register()
    {
        if (count($_POST) > 0) {

            //1. Validate the input
            $this->load->helper(array('form', 'url'));
    		$this->load->library('form_validation');
    
    		$this->form_validation->set_rules('firstName', 'First Name',    'required|min_length[3]|max_lenth[75]');
    		$this->form_validation->set_rules('lastName',  'Last Name',     'required|min_length[3]|max_length[75]|alpha');
    		$this->form_validation->set_rules('email',     'Email Address', 'required');
    
    		if ($this->form_validation->run() == TRUE)
    		{
                //2. We will add it to the database
    			$this->load->model('student_model');
    			$this->student_model->createStudent($_POST['firstName'], $_POST['lastName'], $_POST['email']);
    			
    			//3. Redirect to a page with a success message here
    			// Old way: header("Location: pagename.php")
    			redirect('students/index');
    			return;
    		}

        }

        load_a_page('students_register');
    }

}

/* End of file Students.php */
/* Location: ./application/controllers/Students.php */