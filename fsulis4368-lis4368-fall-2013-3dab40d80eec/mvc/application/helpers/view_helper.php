<?php

function load_a_page($which_page, $viewData = array())
{
    //Make CodeIgniter available inside of this function
    $ci =& get_instance();
    
    //Pass the courses array to a view to printed in HTML
    $ci->load->view('includes/header');
    $ci->load->view($which_page, $viewData);
    $ci->load->view('includes/footer');    
}

?>