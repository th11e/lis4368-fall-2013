<?php

//Data Model
class Book {

    //Class Properties    
    protected $bookTitle, $cover, $edition, $author, $description, $isbn, $condition,
            $courseNumber, $cost;

    
    //Magic Model
    public function __construct($bookTitle, $cover, $edition, $author, $description, 
            $isbn, $condition, $courseNumber, $cost) 
    {
        
        if ($bookTitle != null) {
        $this->setBookTitle($bookTitle);
        }
        
        if ($cover != null) {
        $this->setCover($cover); 
        }
        
        if ($edition != null) {
        $this->setEdition($edition);        
        }
        
        if ($author != null) {
        $this->setAuthor($author);
        }
        
        if ($description != null) {
        $this->setDescription($description);
        }
        
        if ($isbn != null) {
        $this->setIsbn($isbn);
        }
        
        if ($condition != null) {
        $this->setCondition($condition);
        }
        
        if ($courseNumber != null) {
        $this->setCourseNumber($courseNumber);
        }
        
        if ($cost != null) {
        $this->setCost($cost);
        }
        
    }
    
    //Interface Methods    
    public function setBookTitle($bookTitle) {
        $this->bookTitle = $bookTitle;
    }

    public function getBookTitle() {
        return $this->bookTitle;
    }

    public function setCover($cover) {
        $this->cover = $cover;
    }

    public function getCover() {
        return $this->cover;
    }
    
    public function setEdition($edition) {
        $this->edition = $edition;
    }

    public function getEdition() {
        return $this->edition;
    }
    
      public function setAuthor($author){
    $this->author = $author;
        
    }
    public function getAuthor()
    {
        return $this->author;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function getDescription() {
        return $this->description;
    }
    
    public function setIsbn($isbn)
    {
        $this->isbn = $isbn;
    }
    
    public function getIsbn()
    {
        return $this->isbn;
    }
    
    public function setCondition($condition){
        $this->condition = $condition;
        
    }
    public function getCondition()
    {
        return $this->condition;
    }

    public function setCourseNumber($courseNumber) {
        $this->courseNumber = $courseNumber;
    }

    public function getCourseNumber() {
        return $this->courseNumber;
    }
    
    public function setCost($cost)
    {
        $this->cost = $cost;
    }
    
    public function getCost()
    {
        return $this->cost;
    }
}
?>


