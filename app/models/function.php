<?php

    //Function that checks the acceptable characters a user can use.
   function checkAcceptChar($str)
   {
        $allowedChars = "ABCDEFGHIJKLMNOPQRSTUVWYZabcdefghijlkmnopqrstuvwxyz -'";
    
        for($i = 0; $i < strlen($str); $i++) {

            $currChar = $str{$i};
            //Finds the position of the first occurrence of a string inside another string.
            $isValid = (strpos($allowedChars, $currChar) !== false);

            if ($isValid === false) {
                return false;
            }
        }
        
        //If made it here
        return true;
   }

   
   //Function that checks if the length of a string is less than the given number
   function checkLengthGreaterThan($str, $checkLength = 2)
   {
       return (strlen($str) > $checkLength);
   }
   
   //Function that checks if the length of a string is less than the given number
   function checkLengthLessThan($str, $checkLength = 75)
   {
       return (strlen($str) < $checkLength);
   }

?>
