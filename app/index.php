<?php

error_reporting(E_ALL);

if(isset($_GET['page']))
{
    $whichPage = $_GET['page'];
}
else {
    $whichPage = 'front';
}

//Tell PHP to use sessions
session_start();

include('models/members.php');
include('models/books.php');
include('model_mappers/membermapper.php');
include('model_mappers/dbconn.php');
$db = new Dbconn('ispace-2013.cci.fsu.edu', 'th11e_booksearch', 'th11e', 'js0wyxn4');

//Includes the header
include('includes/header.php');

if(file_exists('/pages' . $whichPage . '.php'))
{
    include ('/pages' . $whichPage . '.php' );   
}

else
{
    header('HTTP/1.0 404 Not Found');
    include('includes/404.php');
}

include('pages/' . $whichPage . '.php');

echo "You are looking at the " . $whichPage . " page.";


//Includes the footer
include('includes/footer.php');
?>
