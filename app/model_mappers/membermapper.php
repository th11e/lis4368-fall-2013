<?php

class MemberMapper {

    protected $dbconn;

    function __construct($dbconn) {
        $this->dbconn = $dbconn;
    }

    function createMember($memberObj) {
        $conn = new PDO('mysql:host=ispace-2013.cci.fsu.edu; dbname=th11e_booksearch', 'th11e', 'js0wyxn4');


        $memmt = $conn->prepare("INSERT INTO member (firstName, lastName, major, username, "
                . "password, re_password, phoneNumber, email, school) "
                . "VALUES (fname, lname, uname, pword, re_pword, pnum, email, school)");

        $memmt->bindParam(':fname', $memberObj->getFirstName());
        $memmt->bindParam(':lname', $memberObj->getLastName());
        $memmt->bindParam(':uname', $memberObj->getUsername());
        $memmt->bindParam(':pword', $memberObj->getPassword());
        $memmt->bindParam(':re_pword', $memberObj->getrePassword());
        $memmt->bindParam(':pnum', $memberObj->getPhoneNumber());
        $memmt->bindParam(':email', $memberObj->getEmail());
        $memmt->bindParam(':school', $memberObj->getSchool());

        $result = $memmt->execute();

        if ($result === false) {
            
        }
        return $result;
    }

    function retrieveMembers() {
        //Creates a connection by calling a function
        $conn = $this->dbconn->getConnection();
        //SQL statement that retrieves a member from the database
        $result = $conn->query('SELECT * FROM member;');

        //Fetches the class Member
        $result->setFetchMode(PDO::FETCH_CLASS, 'Member');
        $members = $result->fetchAll();
        return $members;
    }

    function retrieveMember($id) {
        //Connect to database by creating PDO object
        $conn = $this->dbconn->getConnection();
        //Run a query that selects a single student 
        $result = $conn->query("SELECT * FROM member WHERE id = $id;");
        //Results from the databse will be converted into Member objects
        $result->setFetchMode(PDO::FETCH_CLASS, 'Member');
        $members = $result->fetch();
        
        return $members;
    }

}

?>
