<?php

class BookMapper {

    protected $dbconn;

    function __construct($dbconn) {
        $this->dbconn = $dbconn;
    }

//create a new search
//Adds a new book to the database
    function createNewBook() {

       $conn =  $this->dbconn->getConnection();
        //A SQL statement that creates an object to place in the database.
        $bookmt = $conn->prepare("INSERT INTO book (bookTitle, cover, edition,"
                . "author, description, isbn, condition, courseNumber, cost) VALUES (:title, :cov,"
                . ":edit, :auth, :descr, :isbn, :cond, :cnum, :cost)");

        $bookmt->bindParam(':title', $BookObj->getBookTitle());
        $bookmt->bindParam(':cov', $BookObj->getCover());
        $bookmt->bindParam(':edit', $BookObj->getEdition());
        $bookmt->bindParam(':auth', $BookObj->getAuthor());
        $bookmt->bindParam(':isbn', $BookObj->getIsbn());
        $bookmt->bindParam(':cond', $BookObj->getCondition());
        $bookmt->bindParam(':cnum', $BookObj->getCourseNumber());
        $bookmt->bindParam(':cost', $BookObj->getCost());
        $result = $bookmt->execute();

        if ($result === false) {
            var_dump($conn->errorCode());
        }

        return $result;
    }

    //Function to retrieve a book
    
    function retrieveBooks() {
        $conn = $this->dbconn->getConnection();
        //SQL statement that retrieves a book from the database
        $result = $conn->query('SELECT * FROM book;');

        //Fetch the class Book
        $result->setFetchMode(PDO::FETCH_CLASS, 'Book');
        $books = $result->fetchAll();

        return $books;
    }

    //Function to retrieve a single book
    function retrieveBook($id) {
        //$conn = $this->$dbconn->connectToDatabase();
        $conn = $this->dbconn->getConnection();
        //SQL statement that retrieves a member from the database
        $result = $conn->query("SELECT * FROM th11e_booksearch.books WHERE id = $id;");

        //Fetch the class Book
        $result->setFetchMode(PDO::FETCH_CLASS, 'Book');
        $book = $result->fetch();

        return $book;
    }

    protected function connectToDatabase() {
        //Connect to database by creating PDO object
        $conn = new PDO('mysql:host=ispace-2013.cci.fsu.edu;dbname=th11e_booksearch', 'th11e', 'js0wyxn4');
        return $conn;
    }

}

?>
