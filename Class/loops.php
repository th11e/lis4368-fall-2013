<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        //For loop
        
        $myInt = 0;
        //While
        while ($myInt < 15)
        {
            echo "<p>myInt is " . $myInt . " </p>";
            $myInt = $myInt + 1;
            
        }
        
       
        //For
        for ($myInt = 0; $myInt < 15; $myInt++)
        {
            echo "<p>myInt is " . $myInt . " </p>";
        }
        
        //Do While
        $myInt = 0;
        do
        {
        echo "<p>myInt is " . $myInt . " </p>";
        $myInt++;
        //Will go through the loop at least once.
        } while (45 < 15) 
            //45 will never be less than 15.
       
         //echo "<p>All done</p>";
        
        ?>
    </body>
</html>
