<?php
include('members.php');
include('books.php');

//Members array
$members = array();

//Instantiating a class into an object and putting it into an array
$members[] = new Member('Kido', 'Huynh', 'kidoH', 'kidoH@my.fsu.edu', 
        'Florida State University');
$members[] = new Member('Marsha', 'Daniels', 'hydramarsh', 'marshad@my.tcc.fl.edu', 
        'Tallahassee Community College');
$members[] = new Member('Karen', 'Candy', 'iAMhungry', 'iAMhungry@gmail.com', 
        'Florida State University');

//Books Array
$books = array();
            
$books[] = new Book('Campbell Biology', 'Hardback', '9th Edition', 'Jane B. Reece', '*description*', 
        'ISBN:0321558146', 'Good', 'BIO2923', '110.00');        
$books[] = new Book('Biology', 'Hardback', '8th Edition', 'Neil A. Campbell', '*description*', 
        'ISBN:0805368442', 'Like New', 'BIO2923', '150');
$books[] = new Book('Biology', 'Paperback', '2nd Edition', 'Robert Brooker', '*description*', 
        'ISBN:0077349962', 'Fair', 'BIO2923', '25.99');

?>
            
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
        <h1>Hello, There!</h1>

<?php
echo "<ul>";

foreach ($members as $mem) {
    echo "<li>" . 
    "<b>First Name:</b> " . $mem->getFirstName() . 
    "<ol><b>Last Name:</b> " . $mem->getLastName() .
    "<br><b>Username:</b> " . $mem->getUserName() . 
    "<br><b>Email:</b> " . $mem->getEmail() .
    "<br><b>School:</b> " . $mem->getSchool() . 
    "<br><br></ol></li>";
}
echo "</ul>";
?>  

        <h1>Hello, Books!</h1>
        <?php
        echo "<ul>";
        foreach ($books as $book) {
            echo "<li>" . 
            "<h3>" . $book->getBookTitle() . "</h3>" .  
            "<ol>" . $book->getAuthor() . " <br>" .
            $book->getCover() .  " <br>" .
            $book->getEdition() .  " <br>" .
            $book->getDescription() . " <br>" .
            $book->getIsbn() . 
            $book->getCondition() . " <br>" .
            $book->getCourseNumber() .  " <br>" .
            "$" . $book->getCost() .
            
            "</ol></li>";
        }
        echo "</ul>";

        ?>              

    </body>
</html>