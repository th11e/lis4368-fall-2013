<?php

class Course
{
    //Properties
    protected $name;
    protected $number;
    protected $location;
    protected $instructor;
    
    protected $roster;
    
    public function __construct($name, $number)
    {
        $this->setName($name);
        $this->setNumber($number);
        
        //Make the roster is an array variable right off the bat
        $this->roster = array();
    }
    
    //Magic Method to allow reading of any class property
    //from outside the class
    public function __get($item)
    {
        return $this->$item;
    }
    
    public function setName($name)
    {
        $this->name = $name;
    }

    public function setNumber($number)
    {
        $this->number = $number;
    }    
    
    public function setInstructor($instructor)
    {
        $this->instructor = $instructor;
    }    
    
    public function setLocation($location)
    {
        $this->location = $location;
    }        
    
    //Forces it to be an object
    //Always send a student
    //So that the roster only contains student objects
    //Type-pending
    public function addStudent(Student $student)
    {
        //if (count($this->roster) > 2)
        $this->roster[] = $student;
    }
}

?>
