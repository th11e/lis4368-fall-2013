<?php
session_start();

if (count($_POST) > 0) {
    //Skip validation.
    $validationSucceeds = true;

    if ($validationSucceeds == true) {

        if ($doesMatch == true) {
            //User has logged in successfully.

            $_SESSION['is_logged_in'] = true;
            echo "You are now logged in!";
            die();
        }
    }
}
?>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Login Page</title>
    </head>
    <body>

        <h1>Login page. Login here.</h1>
<?php
if (isset($_SESSION['is_logged_in'])) {
    unset($_SESSION['is_logged_in']);
    echo "You are now logged out!";
} else {
    echo "You were not logged in yet";
}
?>

    </body>
</html>