<?php

include('models/members.php');
include('model_mappers/membermapper.php');
include('model_mappers/dbconn.php');

//Creates a new connection.
$db = new Dbconn('ispace-2013.cci.fsu.edu', 'th11e_booksearch', 'th11e', 'js0wyxn4');

$mapper = new MemberMapper($db);

$members = $mapper->retrieveMembers();

?>

<!DOCTYPE html>
<html>
<head>
<title>Contact User</title>
<link href="book.css" rel="stylesheet" type="text/css">
</head>
<body>
<?php
        //Including the divbox.php to call header function
        include('divbox.php');
        div();
?>
    
        Subject:<br>
        <input type='text' name='username' size='15'><br><br>
        
        List of user books: <br> <br>
        <select>
            <option value='bio'>Biology 9th Edition [$110.00 OBO]</option>
            <option value='enc'>English Literature for Dummies [$45 OBO]</option>
            <option value='hum'>European Humanities 2nd Edition [$25 OBO]</option>
        </select><br><br>
  
        Body: <br>
            <textarea name='comments' cols='35' rows='10'>Enter your comments here...
            </textarea><br><br>
       <input type='submit' value='Contact'>

       
       <h3>Users who have this book:</h3>
 <?php
        echo "<ul>";
        foreach ($members as $member) {
            echo "<li>" . $member->getFirstName() . " " . $member->getLastName() . "</li>";
        }
        echo "</ul>";

        ?>
       
</div>
    </body>
</html>

