<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Search Results</title>
        <link href="book.css" rel="stylesheet" type="text/css"/>
    </head>

    <body>
        <?php 
        include('divbox.php');
        div();

        $bookSearchResult = array(
                    'Campbell Biology' => array(
                        'cover' => 'Hardback',
                        'edition' => '9th Edition',
                        'author' => 'Jane B. Reece',
                        'description' => '*description*',
                        'isbn' => 'ISBN:0321558146',
                        'condition' => 'Good',
                        'courseNumber' => 'BIO2923',
                        'cost' => 110.00),
                    'Biology ' => array(
                        'cover' => 'Hardback',
                        'edition' => '8th Edition',
                        'author' => 'Neil A. Campbell',
                        'description' => '*description*',
                        'isbn' => 'ISBN:0805368442',
                        'condition' => 'Like New',
                        'courseNumber' => 'BIO2923',
                        'cost' => 150),
                    'Biology' => array(
                        'cover' => 'Paperback',
                        'edition' => '2nd Edition',
                        'author' => 'Robert Brooker',
                        'description' => '*description*',
                        'isbn' => 'ISBN:0077349962',
                        'condition' => 'Fair',
                        'courseNumber' => 'BIO2923',
                        'cost' => 25.99)
        );

        foreach ($bookSearchResult as $bookTitle => $attributes) {
            echo "<p><h3>" . $bookTitle
            . " $" . $attributes['cost'] . "</h3>"
            . "   - <input type='submit' value='Delete'><br>";
            echo $attributes['condition'];
            echo " - " . $attributes['edition'];
            echo " - " . $attributes['author'];
            echo " - " . $attributes['description'];
            echo " - " . $attributes['isbn'];
            echo " - " . $attributes['courseNumber'];
        }
        ?>
</div>
</body>  
</html>
