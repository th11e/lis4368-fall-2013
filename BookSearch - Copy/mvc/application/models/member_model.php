<?php

class Member_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
        
    }
    
    public function retrieveMembers()
    {
        $sql = "SELECT * FROM th11e_booksearch.member";
        
        $this->load->database();
        $query = $this->db->query($sql);
        return $query->result();
    }
    

}