<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Custom secure Controller
 *
 * @package default
 * @author Christopher Imrie
 */
class MY_Controller extends Controller {
    
    /**
     * Secure Constructor
     *
     * 
     */
    function MY_Controller()
    {   
        parent::Controller();
        
        // When someone successfully logs in, I set a session userdata variable of "logged_in".
        // Here we check if it exists
        if(!$this->session->userdata('logged_in')){

            // ====================
            // = Login has failed =
            // ====================

            // If not logged in, record what url they are attempting to acces and show the login form
            $this->session->set_userdata('REDIRECT' , uri_string());

            // Give the browser an Unauthorized HTTP header
            $this->output->set_status_header('401');

            // Redirect to the login page, change this to your login controller/method
            redirect('login');

        }else{
            
            // =======================
            // = Login has succeeded =
            // =======================
            
            //This is optional, but I have a method that sets up some constants
            $this->_login_init();

        }
    }
    // END MY_Controller method
    
    /**
     * Performs all login functionality such as setting up constants
     *
     * @return void
     * @author Christopher Imrie
     **/
    private function _login_init()
    {
        // I have a model designed to fetch user data.  Since I am logging a user in,
        // I am loading it here and fetching the user's data
        $this->load->model("users");
        
        
        // I call a method here to setup some constants to be available to all secure controllers
        if($userdata = $this->users->get_userdata($this->session->userdata('username')))
        {
            //If we have data, then setup the constants or do special functionality in here
            
            
            // ===========
            // = Example =
            // ===========
            
            /**
            * Logged in users email
            **/
            // define('USERNAME', $userdata->username);


        }else{
            // ===========================
            // = Error fetching userdata =
            // ===========================
            
            //Destroy the session
            $this->session->sess_destroy();

            //Log it
            log_message('error', 'Session error.  Session id not found');
            
            //Show it
            show_error('Session error, please log in again.');
        }

    }
    // END _login_init method
}