<h1>Search results:</h1>

<table>
    <tbody>
        <?php foreach ($books as $book) { ?>
            <tr>
                <td><b><?php echo $book->bookTitle; ?></b></td>
                <td><?php echo $book->edition . ' edition' ?></td>
                <td><?php echo $book->cover; ?></td>                    
                <td><?php echo $book->author; ?></td>
                <td><?php echo $book->condition; ?></td>
                <td><?php echo $book->isbn; ?></td>
                <td><?php echo $book->cost; ?></td>
            </tr>
        <?php } ?> 
    </tbody>
</table>