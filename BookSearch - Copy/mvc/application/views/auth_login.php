<h2>Login to our System</h2>

<form method='post'>
    
    <?php echo validation_errors(); ?>
    
    <?php
    
        if (isset($bad_credentials)) {
            echo $bad_credentials;
        }
    ?>
    
    <p>
        <label for='username'>Login</label>
        <input type='text' name='username' id='username' />
    </p>

    <p>
        <label for='password'>Password</label>
        <input type='password' name='password' id='password' />   
    </p>
    
    <p>
        <button type='submit'>Login</button>
    </p>

</form>
