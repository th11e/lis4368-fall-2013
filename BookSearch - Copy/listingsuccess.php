<!DOCTYPE html>
<html>
    <head>
        <title>Success!</title>
        <link href="book.css" rel="stylesheet" type="text/css">
    </head>

    <body>
        <?php
        include('divbox.php');
        div();
        ?>

        <p>Listing success! Click 'Next' to view your listing!</p>
        <input type='submit' value='Next'>
    </div>
</body>
</html>
