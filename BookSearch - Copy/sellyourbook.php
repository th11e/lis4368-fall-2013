<?php

    //Tell PHP we are going to be using sessions
    session_start();

    if (isset($_SESSION['is_logged_in']) == false) {
      
      header("Location: loginpage.php");
      
    }
    
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Sell your book</title>
        <link href="book.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <?php
        //Including the divbox.php to call header function
        include('divbox.php');
        div();
        ?>
        <form action='listing.php' method='post'>
            <p>
                <label>Book Title: <input name='firstName' type='text' /></label>
            </p>

            <p>
                Cover: <select>
                    <option value='set'>Hardcover</option>
                    <option value='set'>Paperback</option>
                    <option value='obo'>Binder</option>
                    <option value='obo'>Other</option>
                </select>
            </p>

            <p>
                <label for='edition_input'>Edition: </label> 
                <input name ='edition' type ='text' id='edition_input' />
            </p>

            <p>
                <label for='author_input'>Author: </label> 
                <input name ='author' type ='text' id='author_input' />
            </p>
            <p>
                <label for='isbn_input'>ISBN: </label> 
                <input name ='isbn' type ='text' id='isbn_input' />
            </p>
            <p>
                <label for='courseNumber_input'>Course Number: </label> 
                <input name ='courseNumber' type ='text' id='courseNumber_input' />
            </p>

            <p>
                Condition: <input type='text' name='coursenumber' size='20'>
                <select>
                    <option value='set'>Like New</option>
                    <option value='set'>Very Good</option>
                    <option value='set'>Good</option>
                    <option value='obo'>Acceptable</option>
                    <option value='obo'>Fair</option>
                </select>
            </p>

            <p>
                <label for='price_input'>Price: </label> 
                <input name ='edition' type ='text' id='price_input' />
                <select>
                    <option value='set'>Set Price</option>
                    <option value='obo'>Or Best Offer</option>
                </select>
                <br><i>'Set Price' will leave no room for the textbook purchaser to lower the
                    price. 'Or Best Offer' will leave room for you to take the best offer 
                    that becomes available.</i>
            </p>
        </form>

        <button type='submit'>Sell my book</button>
    </div>
</body>
</html>
