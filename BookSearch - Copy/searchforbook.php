<!DOCTYPE html>
<html>
    <head>
        <title>Search for a book</title>
        <link href="book.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <?php
        include('models/function.php');

            include('divbox.php');
            div();
            ?>

                <form action='searchresults.php' method='post'>
                    <p>
                        <label for='bookTitle_input'>Book Title: </label> 
                        <input name ='bookTitle' type ='text' id='bookTitle_input' />
                    </p>

                    <p>
                        <label for='edition_input'>Edition: </label> 
                        <input name ='edition' type ='text' id='edition_input' />
                    </p>

                    <p>
                        <label for='author_input'>Author: </label> 
                        <input name ='school' type ='text' id='author_input' />
                    </p>

                    <p>
                        <label for='isbn_input'>ISBN: </label> 
                        <input name ='isbn' type ='text' id='isbn_input' />
                    </p>

                    <p>
                        <label for='courseNumber_input'>Course Number: </label> 
                        <input name ='courseNumber' type ='text' id='courseNumber_input' />
                    </p>

                    <button type='submit'>Search</button>
                </form>

    </div>
</body>
</html>
