<!DOCTYPE HTML>
<html>
    <head>
        <title>Logging out...</title>
    </head>
    <body>

        <?php
        session_start();
        session_destroy();
        ?>
        <?php
        //Logout page that redirects to the front page.
        if (isset($_SESSION['is_logged_in'])) {
            unset($_SESSION['is_logged_in']);
            echo "<p>You are now logged out!</p>";


            header('refresh: 2; url=loginpage.php');
            echo "You are being redirected to the front page...";
        } else {
            echo "<p>You have to be logged in before being logged out!</p>";
            header('refresh: 2; url=loginpage.php');
            echo "You are being redirected to the front page...";
        }
        ?>
    </body>
</html>