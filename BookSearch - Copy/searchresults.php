<?php
include('models/books.php');
include('model_mappers/bookmapper.php');
include('model_mappers/dbconn.php');

//Creates a new connection.
$db = new Dbconn('ispace-2013.cci.fsu.edu', 'th11e_booksearch', 'th11e', 'js0wyxn4');

$mapper = new BookMapper($db);
$books = $mapper->retrieveBooks();

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Search Results</title>
        <link href="book.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <?php
    //Including the divbox.php to call header function
        include('divbox.php');
        div();
        ?>

        <h1>Books for sale: </h1>
        <?php
        
        foreach ($books as $book) {
            
            echo "<h3>" . $book->getBookTitle() . "</h3>";
            echo "<ul>" .
            "<li>Author: " . $book->getAuthor() . "</li>" .
            "<li>Cover Type: " . $book->getCover() . "</li>" .
            "<li>Edition: " . $book->getEdition() . "</li>" .
            "<li>Description: " . $book->getDescription() . "</li>" .
            "<li>" . $book->getIsbn() . "</li>" . 
            "<li>Condition: " . $book->getCondition() . "</li>" . 
            "<li>Course Number: " . $book->getCourseNumber() . "</li>" .
           "<li>Cost: " .  "$" . $book->getCost() .
            "</ul>";
            
            echo "<input type='submit' value='Contact user for this book'>";
        }
        
        ?>  
       
        
    </div>
    </body>
</html
